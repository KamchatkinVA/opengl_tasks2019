set(COMMON_DIR ${PROJECT_SOURCE_DIR}/../repo/common)
include_directories(${COMMON_DIR})

set(SRC_FILES
    main.cpp
    ${COMMON_DIR}/DebugOutput.cpp
    ${COMMON_DIR}/Camera.cpp
    ${COMMON_DIR}/Application.cpp
    ${COMMON_DIR}/Mesh.cpp
    ${COMMON_DIR}/ShaderProgram.cpp
)

add_compile_options(-DGLM_ENABLE_EXPERIMENTAL)

MAKE_OPENGL_TASK(591Belkin 1 "${SRC_FILES}")

if (UNIX)
    target_link_libraries(591Belkin1 stdc++fs)
endif()
