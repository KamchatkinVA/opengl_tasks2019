#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>

#include <cmath>
#include <algorithm>

class SteinerSurface : public Application
{
public:
    MeshPtr _surface;
    ShaderProgramPtr _shader;

    int _period = 25;
    float _radius = 1;

    glm::vec3 get_cords(float u, float v) {
        float x = (_radius * _radius / 2) * (sin(2.0 * u) * cos(v) * cos(v));
        float y = (_radius * _radius / 2) * (sin(u) * sin(2.0 * v));
        float z = (_radius * _radius / 2) * (cos(u) * sin(2.0 * v));
        return glm::vec3(x, y, z);
    }

    glm::vec3 normal(float u, float v) {
        float dx_du = 2.0 * cos(2.0 * u) * cos(v) * cos(v);
        float dx_dv = -sin(2.0 * u) * sin(2.0 * v);

        float dy_du = cos(u) * sin(2.0 * v);
        float dy_dv = 2.0 * sin(u) * cos(2.0 * v);

        float dz_du = -sin(u) * sin(2.0 * v);
        float dz_dv = 2.0 * cos(u) * cos(2.0 * v);

        glm::vec3 a = glm::vec3(dx_du, dy_du, dz_du);
        glm::vec3 b = glm::vec3(dx_dv, dy_dv, dz_dv);
        glm::vec3 norm = glm::cross(a, b);
        norm = glm::normalize(norm);
        return norm;
    }

    MeshPtr makeSteinerSurface(int N) {
        std::vector<glm::vec3> vertices;
        std::vector<glm::vec3> normals;

        for (unsigned int i = 0; i < N; i++)
        {
            float v_from = glm::pi<float>() * i / N;
            float v_to = glm::pi<float>() * (i + 1) / N;

            for (unsigned int j = 0; j < N; j++)
            {
                float u_from = glm::pi<float>() * j / N;
                float u_to = glm::pi<float>() * (j + 1) / N;

                vertices.push_back(get_cords(u_from, v_from));
                vertices.push_back(get_cords(u_to, v_to));
                vertices.push_back(get_cords(u_to, v_from));

                normals.push_back(normal(u_from, v_from));
                normals.push_back(normal(u_to, v_to));
                normals.push_back(normal(u_to, v_from));

                vertices.push_back(get_cords(u_from, v_from));
                vertices.push_back(get_cords(u_from, v_to));
                vertices.push_back(get_cords(u_to, v_to));

                normals.push_back(normal(u_from, v_from));
                normals.push_back(normal(u_from, v_to));
                normals.push_back(normal(u_to, v_to));
            }
        }

        DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

        DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

        MeshPtr mesh = std::make_shared<Mesh>();
        mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
        mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);

        mesh->setPrimitiveType(GL_TRIANGLES);
        mesh->setVertexCount(vertices.size());

        mesh->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.5f)));

        return mesh;
    }

    void makeScene() override
    {
        Application::makeScene();

        //Включает сглаживание точек
        glEnable(GL_POINT_SMOOTH);
        //Включает изменение размера точек через шейдер
        glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);

        _surface = makeSteinerSurface(_period);

        _shader = std::make_shared<ShaderProgram>();
        _shader->createProgram("./593KamchatkinData1/shader.vert", "./593KamchatkinData1/shader.frag");
    }

    void update() override
    {
        Application::update();
    }

    void draw() override
    {
        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Подключаем шейдер
        _shader->use();

        //Загружаем на видеокарту значения юниформ-переменные: время и матрицы
        _shader->setFloatUniform("time", (float)glfwGetTime()); //передаем время в шейдер

        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        //Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
        _shader->setMat4Uniform("modelMatrix", _surface->modelMatrix());
        _surface->draw();

    }

    void handleKey(int key, int scancode, int action, int mods) override
    {
        if (key == GLFW_KEY_MINUS)
        {
            _period -= 1;
            _period = std::max(3, _period);
            
            _surface = makeSteinerSurface(_period);
        }
        if (key == GLFW_KEY_EQUAL)
        {
            _period += 1;

            _surface = makeSteinerSurface(_period);
        }

        Application::handleKey(key, scancode, action, mods);
    }
};

int main()
{
    SteinerSurface app;
    app.start();
    return 0;
}
